Simple telegram bot on Node.js providing functionality of integer numbers calculator, with +/- operations.

#Usage:
```bash
node psycalcbot.js
```

## License

**Apache 2.0**

Copyright 2018 shirvalx