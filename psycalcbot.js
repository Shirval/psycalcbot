const https = require('https');
https.post = require('https-post');
const events = require('events');
const propertiesReader = require('properties-reader');
const express = require('express');
const bodyParser = require('body-parser');

const properties = propertiesReader('app.properties');
const appPort = properties.get('appPort');
const token = properties.get('token');
const tgHost = properties.get("tgHost");
const tgPrefix = properties.get("tgPrefix");
const tgAddress = tgHost + tgPrefix;

const keyboard = {
    inline_keyboard : [[{'text':'AC', 'callback_data' : 'AC'}, {'text':'+', 'callback_data' : '+'}, {'text':'-', 'callback_data' : '-'}],
                       [{'text':'7', 'callback_data' : '7'},   {'text':'8', 'callback_data' : '8'}, {'text':'9', 'callback_data' : '9'}],
                       [{'text':'4', 'callback_data' : '4'},   {'text':'5', 'callback_data' : '5'}, {'text':'6', 'callback_data' : '6'}],
                       [{'text':'1', 'callback_data' : '1'},   {'text':'2', 'callback_data' : '2'}, {'text':'3', 'callback_data' : '3'}],
                       [{'text':'0', 'callback_data' : '0'},   {'text':'=', 'callback_data' : '='} ]]};

const eventEmitter = new events.EventEmitter();
const state = new Map();

function UserState(currentNum, previousNum, action) {
    this.currentNum = currentNum;
    this.previousNum = previousNum;
    this.lastAction = action;
}
UserState.prototype.isComputable = function () {
    return this.currentNum && this.previousNum && this.lastAction && this.lastAction !== '=';
};
UserState.prototype.reset = function() {
    this.currentNum = '';
    this.previousNum = '';
    this.lastAction = '';
};

var computeResult = function(first, second, action) {
    if (action === '+') {
        return Number(first) + Number(second);
    }
    if (action === '-') {
        return Number(first) - Number(second);
    }
};

var computeAndReturn = function (userState, callback_query) {
    var result = computeResult(userState.previousNum, userState.currentNum, userState.lastAction);
    userState.reset();
    eventEmitter.emit('updateMessage', callback_query, result);
    return result;
};

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended : true
}));

app.get('/healthcheck', function(req, resp) {
    resp.send('alive');
});

app.post('/push-me/'+token, function(req, resp) {

    var update = req.body;
    console.log('bot got push: ' + JSON.stringify(update));
    eventEmitter.emit('tgUpdate', update);

    resp.end('ok');

});

app.listen(process.env.PORT || appPort);

var tgResponseMonitor = function(resp, name) {
    var data = '';

    resp.on('data', function(chunk) {
        data+=chunk;
    });

    resp.on('end', function() {
        var resp = JSON.parse(data);
        if (!resp.ok) {
            console.log("error in " + name + ", response: " + data);
        }
    })
};

eventEmitter.on('tgUpdate', function (update) {
        var text;
        if (update.message) {
            text = update.message.text;
            if (text === '/start') {
                eventEmitter.emit("startCommand", update);
            }
            return;
        }
        if (update.callback_query) {
            text = update.callback_query.data;

            if (!isNaN(text)) {
                eventEmitter.emit('gotNumber', update.callback_query);
                return;
            }
            if (text === '+' ||
                text === '-' ||
                text === '=' ||
                text === 'AC') {
                eventEmitter.emit('gotAction', update.callback_query)
            }
        }
});

eventEmitter.on("startCommand", function(update) {
    var fromId = update.message.from.id;
    state.delete(fromId);
    var body = {
        chat_id : update.message.chat.id,
        text : '0',
        reply_markup : JSON.stringify(keyboard)
    };

    https.post(tgAddress + "/sendMessage", body, function(resp) {
        tgResponseMonitor(resp, '/sendMessage')
    })
    .on('error', function(err) {
        console.log(err);
    })
});

eventEmitter.on('gotAction', function(callback_query) {
    var fromId = callback_query.from.id;
    var action = callback_query.data;
    var userState = state.get(fromId);
    if (userState) {
        var result;
        if (action === 'AC') {
            state.delete(fromId);
            eventEmitter.emit('updateMessage', callback_query, '0');
        } else if (action === '=') {
            if (userState.isComputable()) {
                result = computeAndReturn(userState, callback_query);
                userState.currentNum = result.toString();
            }
        } else if (userState.isComputable()) {
            result = computeAndReturn(userState, callback_query);
            userState.previousNum = result.toString();
        } else if (userState.currentNum && !userState.previousNum) {
            userState.previousNum = userState.currentNum;
            userState.currentNum = '';
        }
        userState.lastAction = action;
    }
});

eventEmitter.on('gotNumber', function(callback_query) {
   var fromId = callback_query.from.id;
   var number = callback_query.data;
   var userState = state.get(fromId);
   if (!userState) {
       userState = new UserState(number, '', '');
       state.set(fromId, userState);
       eventEmitter.emit('updateMessage', callback_query, number);
   } else {
       if (userState.lastAction === '=') {
           userState.reset();
       }
       userState.currentNum+=number;
       eventEmitter.emit('updateMessage', callback_query, userState.currentNum);
   }
});

eventEmitter.on('updateMessage', function(callback_query, newText) {
   var body = {
       chat_id : callback_query.message.chat.id,
       message_id : callback_query.message.message_id,
       text : newText,
       reply_markup : JSON.stringify(keyboard)
   };
   https.post(tgAddress + "/editMessageText", body, function(resp) {
       tgResponseMonitor(resp, '/editMessageText');
   }).on('error', function(err) {
       console.log(err);
   })
});